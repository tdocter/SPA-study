# -*- mode: ruby -*-
# vi: ft=ruby

# file   : Vagrantfile
# purpose: deploy SaltStack/Puppet/Ansible study environment.
#
# author : tycho alexander docter
# date   : 2017/12/26
# version: v1.0

VAGRANTFILE_API_VERSION = "2"
Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|

    # disable vagrant-vbguest auto update
    if defined? VagrantVbguest
        config.vbguest.auto_update = false
    end

    ENV["LC_ALL"] = "en_US.UTF-8"
    ENV["LANG"] = "en_US.UTF-8"

    # ansible services
    config.vm.define "ansible" do |ansible|
        ansible.vm.box = "centos/7"
        ansible.vm.hostname = "ansible.doctert.com"
        ansible.vm.network "private_network", ip: "10.11.12.4"

        ansible.vm.provider "virtualbox" do |vbox|
            vbox.customize [
                "modifyvm", :id,
                "--memory", 512,
                "--cpus", 1,
                "--name", "ansible",
            ]
        end
    end

    # puppet services
    config.vm.define "puppet" do |puppet|
        puppet.vm.box = "centos/7"
        puppet.vm.hostname = "puppet.doctert.com"
        puppet.vm.network "private_network", ip: "10.11.12.5"

        puppet.vm.provider "virtualbox" do |vbox|
            vbox.customize [
                "modifyvm", :id,
                "--memory", 512,
                "--cpus", 1,
                "--name", "puppet",
            ]
        end
    end

    # Salt services
    config.vm.define "salt" do |salt|
        salt.vm.box = "centos/7"
        salt.vm.hostname = "salt.doctert.com"
        salt.vm.network "private_network", ip: "10.11.12.6"

        salt.vm.provider "virtualbox" do |vbox|
            vbox.customize [
                "modifyvm", :id,
                "--memory", 512,
                "--cpus", 1,
                "--name", "salt",
            ]
        end
    end
end
